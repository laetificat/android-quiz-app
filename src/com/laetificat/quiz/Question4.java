package com.laetificat.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Question4 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question_radio);
		
		final TextView vraag = (TextView) findViewById(R.id.vraag);
		Button antwoordKnop = (Button) findViewById(R.id.antwoordKnop);
		//final EditText antwoord = (EditText) findViewById(R.id.antwoord);
		final RadioGroup antwoordGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		final RadioButton antwoord1 = (RadioButton) findViewById(R.id.radio0);
		final RadioButton antwoord2 = (RadioButton) findViewById(R.id.radio1);
		final RadioButton antwoord3 = (RadioButton) findViewById(R.id.radio2);
		
		final Intent intent = getIntent();
		final String name = intent.getStringExtra("naamData");
		final Integer score = intent.getIntExtra("scoreData", 0);
		
		antwoord1.setText("3");
		antwoord2.setText("1");
		antwoord3.setText("2");
		
		vraag.setText("Het antwoord is 2");
		
		antwoordKnop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intent = new Intent(Question4.this, Endscreen.class);
				
				switch(antwoordGroup.getCheckedRadioButtonId()) {
				
					case R.id.radio0:
						Toast.makeText(getApplicationContext(), "Dat is fout!", Toast.LENGTH_SHORT).show();
						intent.putExtra("naamData", name);
						startActivity(intent);
						break;
					
					case R.id.radio1:
						Toast.makeText(getApplicationContext(), "Dat is fout!", Toast.LENGTH_SHORT).show();
						intent.putExtra("naamData", name);
						startActivity(intent);
						break;
					
					case R.id.radio2:
						Integer scoreUp = score + 5;
						Toast.makeText(getApplicationContext(), "Dat is goed! + " + scoreUp + " punten!", Toast.LENGTH_SHORT).show();
						intent.putExtra("naamData", name);
						intent.putExtra("scoreData", scoreUp);
						startActivity(intent);
						break;
					
					default:
						break;
				
				}
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        MenuItem SNsettings = (MenuItem) menu.findItem(R.id.action_settings);
        SNsettings.setIntent(new Intent(this, Settings.class));
        
        return true;
    }
	
}
