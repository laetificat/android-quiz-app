package com.laetificat.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Question3 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question_radio);
		
		final TextView vraag = (TextView) findViewById(R.id.vraag);
		Button antwoordKnop = (Button) findViewById(R.id.antwoordKnop);
		//final EditText antwoord = (EditText) findViewById(R.id.antwoord);
		final RadioGroup antwoordGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		final RadioButton antwoord1 = (RadioButton) findViewById(R.id.radio0);
		final RadioButton antwoord2 = (RadioButton) findViewById(R.id.radio1);
		final RadioButton antwoord3 = (RadioButton) findViewById(R.id.radio2);
		
		antwoord1.setText("Goofy");
		antwoord2.setText("Mickey Mouse");
		antwoord3.setText("Bugs Bunny");
		
		vraag.setText("Welke Disney mascotte is het bekendst?");
		
		antwoordKnop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent intentGet = getIntent();
				final String name = intentGet.getStringExtra("naamData");
				Integer score = intentGet.getIntExtra("scoreData", 0);
				Intent intent = new Intent(Question3.this, Question4.class);
				
				switch(antwoordGroup.getCheckedRadioButtonId()) {
					
					case R.id.radio0:
						Toast.makeText(getApplicationContext(), "Dat is fout!", Toast.LENGTH_SHORT).show();
						intent.putExtra("naamData", name);
						startActivity(intent);
						break;
					
					case R.id.radio1:
						Integer scoreUp = score + 5;
						Toast.makeText(getApplicationContext(), "Dat is goed! + " + scoreUp + " punten!", Toast.LENGTH_SHORT).show();
						intent.putExtra("scoreData", scoreUp);
						intent.putExtra("naamData", name);
						startActivity(intent);
						break;
					
					case R.id.radio2:
						Toast.makeText(getApplicationContext(), "Dat is fout!", Toast.LENGTH_SHORT).show();
						intent.putExtra("naamData", name);
						startActivity(intent);
						break;
					
					default:
						break;
				
				}
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        MenuItem SNsettings = (MenuItem) menu.findItem(R.id.action_settings);
        SNsettings.setIntent(new Intent(this, Settings.class));
        
        return true;
    }
	
}
