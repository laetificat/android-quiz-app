package com.laetificat.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Question2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question);
		
		final TextView vraag = (TextView) findViewById(R.id.vraag);
		Button antwoordKnop = (Button) findViewById(R.id.antwoordKnop);
		final EditText antwoord = (EditText) findViewById(R.id.antwoord);
		
		Intent intent = getIntent();
		final String name = intent.getStringExtra("naamData");
		final Integer score = intent.getIntExtra("scoreData", 0);
		
		vraag.setText("Wanneer kwam de eerste Star Wars film uit?");
		
		antwoordKnop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (antwoord.getText().toString().toLowerCase().trim().equals("1977")) {
					Integer scoreUp = score + 10;
					Toast.makeText(getApplicationContext(), "Dat is goed! +" + scoreUp + " punten!", Toast.LENGTH_SHORT).show();
					Intent intent = new Intent(Question2.this, Question3.class);
					intent.putExtra("naamData", name);
					intent.putExtra("scoreData", scoreUp);
					startActivity(intent);
				}
				else if (antwoord.getText().toString().toLowerCase().trim().equals("")) {
					Toast.makeText(getApplicationContext(), "Vul iets in!", Toast.LENGTH_SHORT).show();
					antwoord.setText("");
				}
				else {
				Toast.makeText(getApplicationContext(), "Dat is fout!", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(Question2.this, Question3.class);
				intent.putExtra("naamData", name);
				startActivity(intent);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        MenuItem SNsettings = (MenuItem) menu.findItem(R.id.action_settings);
        SNsettings.setIntent(new Intent(this, Settings.class));
        
        return true;
    }
	
}
