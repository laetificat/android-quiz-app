package com.laetificat.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Endscreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.endscreen);
		
		TextView scoreTxt = (TextView) findViewById(R.id.textView2);
		TextView naamTxt = (TextView) findViewById(R.id.textView1);
		
		Intent intent = getIntent();
		String naam = intent.getStringExtra("naamData");
		Integer score = intent.getIntExtra("scoreData", 0);

		scoreTxt.setText("Jou eindscore is " + score + " punten!");
		naamTxt.setText(""+naam);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        MenuItem SNsettings = (MenuItem) menu.findItem(R.id.action_settings);
        SNsettings.setIntent(new Intent(this, Settings.class));
        
        return true;
    }
	
}
